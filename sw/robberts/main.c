/*
Run the example:
$ make
$ ./robberts files/lena_256.bmp files/lena_out_c.bmp files/lena_out_acc.bmp

Create a desired image format (ImageMagick):
$ convert -resize 256x256 -colorspace Gray lena.png lena.bmp
*/

#include "cbmp.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

#define HW_REGS_BASE 0xff200000
#define HW_REGS_SPAN 0x00200000
#define HW_REGS_MASK (HW_REGS_SPAN - 1)
#define ONCHIP_BASE 0x00000000
#define ACCELERATOR_BASE 0x00080030
#define COUNTER_BASE 0x00080000

volatile unsigned int *accel_addr = NULL;
volatile unsigned int *onchip_addr = NULL;
volatile unsigned int *counter_addr = NULL;

int fd;
void *virtual_base;

int map_fpga_memory() {
  // Open /dev/mem
  if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
    printf("ERROR: could not open \"/dev/mem\"...\n");
    return 1;
  }

  // Get virtual addr that maps to physical
  virtual_base = mmap(NULL, HW_REGS_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED,
                      fd, HW_REGS_BASE);
  if (virtual_base == MAP_FAILED) {
    printf("ERROR: mmap() failed...\n");
    close(fd);
    return 1;
  }

  onchip_addr =
      (unsigned int *)(virtual_base + ((ONCHIP_BASE) & (HW_REGS_MASK)));
  accel_addr =
      (unsigned int *)(virtual_base + ((ACCELERATOR_BASE) & (HW_REGS_MASK)));
  counter_addr =
      (unsigned int *)(virtual_base + ((COUNTER_BASE) & (HW_REGS_MASK)));

  return 0;
}

int unmap_fpga_memory() {
  if (munmap(virtual_base, HW_REGS_SPAN) != 0) {
    printf("ERROR: munmap() failed...\n");
    close(fd);
    return 1;
  }
  close(fd);

  return 0;
}

int normalise_output(int **output, BMP *bmp) {
  unsigned int x, y;
  unsigned int temp;
  int min_num = 10000;
  int max_num = -10000;
  float scale;
  unsigned int width = get_width(bmp);
  unsigned int height = get_height(bmp);

  // Normalising
  for (x = 0; x < width; x++) {
    for (y = 0; y < height; y++) {
      temp = output[x][y];
      if (temp < min_num) {
        min_num = temp;
      }
      if (temp > max_num) {
        max_num = temp;
      }
    }
  }

  // Write to the image
  for (x = 0; x < width; x++) {
    for (y = 0; y < height; y++) {
      temp = output[x][y];
      scale = (temp - min_num) / (float)max_num;
      temp = (int)(scale * 255);
      set_pixel_rgb(bmp, x, y, temp, temp, temp);
    }
  }

  return 0;
}

inline void tick() {
  *(counter_addr + 1) = 1;
  *(counter_addr + 2) = 1;
}

inline void tock() { *(counter_addr + 3) = 1; }

inline int timediff() { return *(counter_addr + 0); }

int main(int argc, char *argv[]) {
  if (argc <= 3) {
    printf("Missing input and/or output image\n");
    return 1;
  }

  BMP *bmp_c = bopen(argv[1]);
  BMP *bmp_acc = bopen(argv[1]);

  unsigned int x, y, width, height;
  unsigned char r, g, b;
  unsigned char num1, num2, num3, num4;
  unsigned int i, j, inner_i, inner_j, kernel_index, temp = 0;
  int sum;

  int kernel[] = {1, 1, -1, -1};

  width = get_width(bmp_c);
  height = get_height(bmp_c);
  int *c_output[height];
  int *acc_output[height];

  // Open /dev/mem
  map_fpga_memory();

  // Allocate memory for temp image
  for (i = 0; i < height; i++) {
    c_output[i] = (int *)malloc(width * sizeof(int));
    acc_output[i] = (int *)malloc(width * sizeof(int));
  }

  // Apply C convolution
  tick();
  for (x = 0; x < width; x++) {
    for (y = 0; y < height; y++) {
      if (x == width - 1 || y == height - 1) {
        set_pixel_rgb(bmp_c, x, y, 0, 0, 0);
      } else {
        sum = 0;
        kernel_index = 0;
        for (inner_i = x; inner_i < x + 2; inner_i++) {
          for (inner_j = y; inner_j < y + 2; inner_j++) {
            get_pixel_rgb(bmp_c, inner_i, inner_j, &r, &g, &b);
            sum = sum + (r * kernel[kernel_index] / 2);
            kernel_index++;
          }
        }
        c_output[x][y] = sum;
      }
    }
  }
  tock();
  printf("Execution time of convolution implemented in C is %d cycles "
         "(measured with FPGA clock)\n",
         timediff());

  // Store to onchip
  i = 0;
  for (x = 0; x < width - 1; x++) {
    for (y = 0; y < height - 1; y++) {
      get_pixel_rgb(bmp_acc, x, y, &r, &g, &b);
      num1 = r / 2;

      get_pixel_rgb(bmp_acc, x, y + 1, &r, &g, &b);
      num2 = r / 2;

      get_pixel_rgb(bmp_acc, x + 1, y, &r, &g, &b);
      num3 = r / 2;

      get_pixel_rgb(bmp_acc, x + 1, y + 1, &r, &g, &b);
      num4 = r / 2;

      *(onchip_addr + i) = 0;
      *(onchip_addr + i) = (num1 << 24) | (num2 << 16) | (num3 << 8) | num4;
      i = i + 1;
    }
  }

  // Fire accelerator
  *(accel_addr + 0) = 0;
  *(accel_addr + 1) = i;
  *(accel_addr + 2) = 1;

  tick();
  while (*(accel_addr + 2) != 2)
    ;
  tock();
  printf("Execution time of convolution implemented in FPGA is %d cycles "
         "(measured with FPGA clock)\n",
         timediff());

  // Read results from accelerator
  i = 0;
  for (x = 0; x < width - 1; x++) {
    for (y = 0; y < height - 1; y++) {
      acc_output[x][y] = *(onchip_addr + i);
      if (acc_output[x][y] > 127) {
        acc_output[x][y] -= 256;
      }
      i = i + 1;
    }
  }

  // Normalise outputs
  normalise_output(c_output, bmp_c);
  normalise_output(acc_output, bmp_acc);

  // Write result images
  bwrite(bmp_c, argv[2]);
  bwrite(bmp_acc, argv[3]);

  // Free the memory
  bclose(bmp_c);
  bclose(bmp_acc);
  for (i = 0; i < height; i++) {
    free(acc_output[i]);
    free(c_output[i]);
  }
  unmap_fpga_memory();

  return 0;
}
