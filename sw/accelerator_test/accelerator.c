#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define HW_REGS_BASE ( 0xff200000 )
#define HW_REGS_SPAN ( 0x00200000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define ONCHIP_BASE 0x00000000
#define ACCELERATOR_BASE 0x00080000

int main(void)
{
    volatile unsigned int *accel_addr = NULL;
	volatile unsigned int *onchip_addr = NULL;
	volatile unsigned int dummy;
	void *virtual_base;
	int fd;
	int i;    
    int res;

    // Open /dev/mem
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}
    
    // get virtual addr that maps to physical
	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap() failed...\n" );
		close( fd );
		return(1);
	}


	onchip_addr = (unsigned int *)(virtual_base + (( ONCHIP_BASE ) & ( HW_REGS_MASK ) ));
	accel_addr = (unsigned int *)(virtual_base + (( ACCELERATOR_BASE ) & ( HW_REGS_MASK ) ));

	printf("Start address %x, length %x, status %d\n", *(accel_addr + 0), *(accel_addr + 1), *(accel_addr + 2));


        #define START 0
	
	*(onchip_addr + START + 0) = 0x11F0A0FA;
	*(onchip_addr + START + 1) = 0x11F0A0FB;
	*(onchip_addr + START + 2) = 0x11F0A0FC;

	*(accel_addr + 0) = START;
	*(accel_addr + 1) = 3;
	*(accel_addr + 2) = 0xff;
        res = *(accel_addr + 2);


	printf("Start address %x, length %x, status %d\n", *(accel_addr + 0), *(accel_addr + 1), res);

       printf("Start address %x, length %x, status %d\n", *(accel_addr + 0), *(
accel_addr + 1), *(accel_addr + 2));	
    
	printf("%x %x\n", *(onchip_addr + START + 1), *(onchip_addr + START + 2));
	if( munmap( virtual_base, HW_REGS_SPAN ) != 0 ) {
		printf( "ERROR: munmap() failed...\n" );
		close( fd );
		return( 1 );

	}
	close( fd );
	return 0;
}
