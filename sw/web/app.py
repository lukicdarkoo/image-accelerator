from flask import Flask, request, send_file, make_response, jsonify, session, send_from_directory
import os
from random import randint


app = Flask(__name__)


@app.route('/image_orig', methods=['GET'])
def image_orig():
    return send_file('original.bmp')


@app.route('/image_c', methods=['GET'])
def image_c():
    return send_file('out_c.bmp')


@app.route('/image_acc', methods=['GET'])
def image_acc():
    return send_file('out_acc.bmp')


@app.route('/upload', methods=['POST'])
def upload():
    img = request.files['file']
    full_filename = 'original.bmp'
    img.save(full_filename)

    response_output = os.popen('../robberts/robberts %s %s %s' % (
        full_filename,
        'out_c.bmp',
        'out_acc.bmp'
    )).read().replace('\n', '<br />')

    return open('images.html').read().replace('{{respnse_text}}', response_output).replace('{{rand}}', str(randint(0, 999999)))


@app.route('/')
def root():
    return open('index.html').read()


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.debug = True
    app.run(host='0.0.0.0', port=port)
