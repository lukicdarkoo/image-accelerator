### Load SOC to Linux
```
scp hw/quartus/output_file.rbf root@192.168.20.2:/home/root/DE1_SoC_Computer.rbf
```

### Mount filesystem
```
sshfs root@192.168.20.2:/home/root/project sw
```